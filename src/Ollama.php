<?php

namespace Drupal\ollama;

use Drupal\ollama\Form\OllamaConfigForm;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * Ollama API creator.
 */
class Ollama {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * Ollama URL.
   */
  private string $ollama_URL;

  /**
   * Ollama port.
   */
  private string $ollama_port;

  /**
   * Ollama model.
   */
  private string $ollama_model;

  /**
   * The path suffix.
   */
  private string $pathSuffix = '/api/generate';

  /**
   * Constructs a new Ollama object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $this->ollama_URL = $configFactory->get(OllamaConfigForm::CONFIG_NAME)->get('ollama_URL') ?? '';
  }

}