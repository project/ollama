<?php

namespace Drupal\ollama\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure OllamaConfigForm URL access.
 */
class OllamaConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'ollama.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ollama';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['ollama_URL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ollama Server URL'),
      '#description' => $this->t('This version supports <a href="https://www.drupal.org/project/ai_interpolator" target="_blank">AI Interpolator</a> only. </br></br> IP number or subdomain name is enough. It will be completed by the suffix, please check <a href="https://ollama.ai/" target="_blank">Ollama web site</a>.'),
      '#default_value' => $config->get('ollama_URL'),
    ];
    $form['ollama_port'] = [
      '#type' => 'number',
      '#title' => $this->t('Ollama Server port number.'),
      '#description' => $this->t('Please enter the port number you use for Ollama Server.'),
      '#default_value' => $config->get('ollama_port'),
    ];
    $form['ollama_model'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your LLM choice, which is running on Ollama.'),
      '#description' => $this->t('Please load a model from <a href="https://ollama.ai/models" target="_blank">Ollama models</a>.'),
      '#default_value' => $config->get('ollama_model'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   *   Ollama Settings for model and URL.
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('ollama_URL', $form_state->getValue('ollama_URL'))
      ->set('ollama_port', $form_state->getValue('ollama_port'))
      ->set('ollama_model', $form_state->getValue('ollama_model'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

